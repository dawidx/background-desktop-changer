﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Data;
using System.Data.OleDb;
using System.IO;
using Microsoft.Win32;
using System.Diagnostics;

namespace BackgroudDesktopChanger
{
    public partial class Form1 : Form
    {
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern Int32 SystemParametersInfo(
            UInt32 action, UInt32 uParam, String vParam, UInt32 winIni);

        const int SPI_SETDESKWALLPAPER = 20;
        const int SPIF_UPDATEINIFILE = 0x01;
        const int SPIF_SENDWININICHANGE = 0x02;
        public static readonly UInt32 SPI_GETDESKWALLPAPER = 0x73;
        public static readonly int MAX_PATH = 260;
        string save;
        string aass = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);
        string DatabasePath;
        FileInfo[] rgFiles;
        DirectoryInfo di;
        OleDbDataAdapter adapter;
        /// <summary>
        /// Runs the Program on Startup.
        /// </summary>
        /// <param name="RunOnStartup">True to Run on Startup, False to NOT Run on Startup.</param>
        public Form1()
        {
            InitializeComponent();
        }
        public void connect()
        {
            int maxxxxx = max();
            int maxtablerows = maxxxxx + 3;
            string[] filename = new string[maxtablerows];
            int tableid = 0;
            di = new DirectoryInfo(aass);
            rgFiles = di.GetFiles("*.jpg");
            foreach (FileInfo fi in rgFiles)
            {
                filename[tableid] = fi.Name;
                tableid++;
            }

            int number = LosWallpaper(maxxxxx);
            string aa = aass + "\\" + filename[number];
            save = aass + "\\" + filename[number] + ".bmp";

            if (File.Exists(save) == true)
            {
                SetWallpaper(save);
                return;
            }
            else
            {
                using (Image img = Image.FromFile(aa))
                {
                    img.Save(save, System.Drawing.Imaging.ImageFormat.Bmp);
                }
                SetWallpaper(save);
            }
            settobox();
            return;
        }
        public void SetWallpaper(String path)
        {
            SystemParametersInfo(SPI_SETDESKWALLPAPER, 0, path, SPIF_UPDATEINIFILE | SPIF_SENDWININICHANGE);
            return;
        }
        public int LosWallpaper(int max)
        {
            System.Random x = new Random();
            int number = x.Next(0, max);
            label1.Text = number.ToString();
            return number;
        }
        public void Load()
        {
            if (Installed() == true)
            {
                label3.Text = "Installed";
            }
            else
            {
                label3.Text = "UnInstalled";
                Install();
            }
            return;
        }
        public void Form1_Load(object sender, EventArgs e)
        {
            Load();
        }
        public void settobox()
        {
            pictureBox1.ImageLocation = GetWallpaper();
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            return;
        }
        public void button1_Click(object sender, EventArgs e)
        {
            connect();
        }

        public String GetWallpaper()
        {
            String wallpaper = new String('\0', MAX_PATH);
            SystemParametersInfo(SPI_GETDESKWALLPAPER,
                (UInt32)wallpaper.Length, wallpaper, 0);
            wallpaper = wallpaper.Substring(0, wallpaper.IndexOf('\0'));
            return wallpaper;
        }
        public int max()
        {
            int numberoffiles = 0;
            di = new DirectoryInfo(aass);
            rgFiles = di.GetFiles("*.jpg");
            foreach (FileInfo fi in rgFiles)
            {
                numberoffiles++;
            }
            return numberoffiles;
        }

        private void RunStartup(Boolean RunOnStartup)
        {
            Microsoft.Win32.RegistryKey key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
            if (RunOnStartup == true)
            {
                key.SetValue("oXDawidXo Wallpaper Changer", Directory.GetCurrentDirectory() + "\\Backgroud Desktop Changer.exe");
            }
            else
            {
                key.DeleteValue("oXDawidXo Wallpaper Changer", false);
            }
            return;
        }

        private bool Startup
        {
            get
            {
                return Startup;
            }
            set
            {
                Startup = value;
                Microsoft.Win32.RegistryKey key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
                if (Startup == true)
                {
                    key.SetValue("oXDawidXo Wallpaper Changer", Directory.GetCurrentDirectory().ToString());
                }
                if (Startup == false)
                {
                    key.DeleteValue("oXDawidXo Wallpaper Changer", false);
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SelectWallpaperPath();
        }
        private void wallpaperselect()
        {
            string conString = @"Provider=Microsoft.JET.OLEDB.4.0;" + @"data source=" + Directory.GetCurrentDirectory() + "\\Data";
            // create an open the connection          
            OleDbConnection conn = new OleDbConnection(conString);
            conn.Open();

            // create the DataSet
            DataSet ds = new DataSet();
            string query = "SELECT * FROM WAP WHERE WAP.namee=\"WAP\"";
            // create the adapter and fill the DataSet
            adapter = new OleDbDataAdapter(query, conn);

            adapter.Fill(ds);

            // close the connection
            conn.Close();
            DataTable dt = ds.Tables[0];
            foreach (DataRow dr in dt.Rows)
            {
                if (dr["valuee"].ToString() == "")
                {
                    SelectWallpaperPath();
                }
                else
                {
                    aass = dr["valuee"].ToString();
                }
            }
            return;
        }
        public void SelectWallpaperPath()
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                string path = folderBrowserDialog1.SelectedPath;
                string conString = @"Provider=Microsoft.JET.OLEDB.4.0;" + @"data source=" + Directory.GetCurrentDirectory() + "\\Data";
                // create an open the connection          
                OleDbConnection conn = new OleDbConnection(conString);
                conn.Open();

                // create the DataSet
                DataSet ds = new DataSet();
                string query = "UPDATE WAP SET valuee=\"" + path + "\" WHERE WAP.namee=\"WAP\"";
                // create the adapter and fill the DataSet
                adapter = new OleDbDataAdapter(query, conn);

                adapter.Fill(ds);

                // close the connection
                conn.Close();
                aass = path;
                aass += "\\";
                label3.Text = "Installed";
            }
            return;
        }
        private void Install()
        {
            InstallApplication();
            wallpaperselect();
            return;
        }
        private bool Installed()
        {
            string conString = @"Provider=Microsoft.JET.OLEDB.4.0;" + @"data source=" + Directory.GetCurrentDirectory() + "\\Data";
            // create an open the connection          
            OleDbConnection conn = new OleDbConnection(conString);
            conn.Open();

            // create the DataSet
            DataSet ds = new DataSet();
            string query = "SELECT * FROM WAP WHERE WAP.namee=\"Install\"";
            // create the adapter and fill the DataSet
            adapter = new OleDbDataAdapter(query, conn);

            adapter.Fill(ds);

            // close the connection
            conn.Close();
            DataTable dt = ds.Tables[0];
            foreach (DataRow dr in dt.Rows)
            {
                if (dr["valuee"].ToString() == "true")
                {
                    wallpaperselect();
                    return true;
                }
                else
                {
                    InstallApplication();
                    return false;
                }
            }
            return false;
        }
        public void InstallApplication()
        {
                //Edit Install Properties
                string conString = @"Provider=Microsoft.JET.OLEDB.4.0;" + @"data source=" + Directory.GetCurrentDirectory() + "\\Data";        
                OleDbConnection conn = new OleDbConnection(conString);
                conn.Open();
                DataSet ds = new DataSet();
                string query = "UPDATE WAP SET valuee=\"true\" WHERE WAP.namee=\"Install\"";
                adapter = new OleDbDataAdapter(query, conn);
                adapter.Fill(ds);
                conn.Close();
                //Variable folderBrowserDialog2.SelectedPath to DatabasePath
                DatabasePath = Directory.GetCurrentDirectory();
                return;
        }
    }
}